// extention/popup.js

console.log("INIT POPUP");

// popup.js

document.getElementById('login').addEventListener('click', () => {
	console.log("CLICKED LOGIN 2");
    chrome.runtime.sendMessage({ action: 'signInWithGoogle' }, response => {
        if (response.success) {
            console.log("User signed in. Token:", response.idToken);
            sendTokenToServer(response.idToken); // Send token to your PHP server.
            document.getElementById('status').textContent = 'Logged In';
        } else {
            console.error("Sign-in failed:", response.error);
            alert("Sign-in failed: " + response.error);
             document.getElementById('status').textContent = 'Sign-in Failed';
        }
    });
});

document.getElementById('signOutButton').addEventListener('click', () => {
    chrome.runtime.sendMessage({ action: 'signOut' }, response => {
        if (response.success) {
            console.log("User signed out.");
            document.getElementById('status').textContent = 'Logged Out';
        } else {
            console.error("Sign out error:", response.error);
            document.getElementById('status').textContent = 'Sign-Out Failed';
        }
    });
});

// Check authentication status on popup load
chrome.runtime.sendMessage({ action: 'checkAuthStatus' }, response => {
    if (response.isLoggedIn) {
        console.log("User is logged in. Token:", response.idToken);
        sendTokenToServer(response.idToken);
        document.getElementById('status').textContent = 'Logged In';
    } else {
        console.log("User is not logged in.");
        document.getElementById('status').textContent = 'Logged Out';
    }
});


function sendTokenToServer(idToken) {
    fetch('https://your-php-server.com/api/verify-token', { // Replace with your server URL
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + idToken
        },
        body: JSON.stringify({ token: idToken })
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok: ' + response.status);
        }
        return response.json();
    })
    .then(data => {
        console.log('Server response:', data);
        // Handle the server response (e.g., store a session ID - NO LONGER IN LOCAL STORAGE).
    })
    .catch(error => {
        console.error('Error sending token to server:', error);
    });
}