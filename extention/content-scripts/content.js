class Api {
	constructor() {
		this.url = 'http://localhost/';
        this.selectionData = {};
	}

	async getExistingBias(href) {
		const response = await fetch(this.url+"index.php?biasUrl="+href, {
			method: "GET",
			headers: {
				"Content-Type": "",
			},
		});

		return response.json();
	}

    setSelectionData() {
        // Get the selected text
        let selection = window.getSelection();
        let range = selection.getRangeAt(0);
        // Get the parent element of the selected text
        let selectionData = {
            elementId: range.commonAncestorContainer.parentElement.id || null,
            xpath: getXPath(range.commonAncestorContainer.parentElement) || null,
            plainText: selection.toString(),
            startOffset: range.startOffset,
            endOffset: range.endOffset,
            html: range.commonAncestorContainer.element,
        }
        this.selectionData = selectionData;
        return selectionData;
    }

	/**
	 * markAsBias - This function will send the selected text to the server to be saved as a bias
	 * @param {*} selection 
	 * @param {*} biasUrl 
	 * @returns
	 */
	async markAsBias(selection, biasUrl) {

		let range = selection.getRangeAt(0);
        // Get the parent element of the selected text
        let selectionData = this.selectionData;
        selectionData.note = document.querySelector('.mab-modal-bias-textarea').value;
        
		const span = document.createElement('span');
        span.className = 'highlight';

		const response = await fetch(this.url + "/index.php", {
			method: "POST",
			headers: {
                "Content-Type": "application/json",
			},
			body: JSON.stringify({
				biasSelection: selectionData,
				biasUrl,
			}),
		});

		return response;
	}
}

function getXPath(element) {
    if (element.id !== '') {
        return 'id("' + element.id + '")';
    }
    if (element === document.body) {
        return element.tagName;
    }

    let ix = 0;
    const siblings = element.parentNode.childNodes;
    for (let i = 0; i < siblings.length; i++) {
        const sibling = siblings[i];
        if (sibling === element) {
            return getXPath(element.parentNode) + '/' + element.tagName + '[' + (ix + 1) + ']';
        }
        if (sibling.nodeType === 1 && sibling.tagName === element.tagName) {
            ix++;
        }
    }
}

/**
 * App functionality 
 * - Search for bias already highlighted
 * - Check for someone highlighting text
 * - Display popup / modal
 * - Submit the bias to the server
 */
(() => {
	const api = new Api();

	const highlightText = (node, text) => {
		const innerHTML = node.innerHTML;
		const regex = new RegExp(text, "g");
		if (node.innerText.match(regex)) {
			const newHTML = innerHTML.replace(
				regex,
				`<span class="mab-bias-highlight">${text}</span>`
			);
			node.innerHTML = newHTML;
		}
	}

	const highlightExistingBias = async () => {
		const response = await api.getExistingBias(window.location.href);

		if (! response) {
			return;
		}

		if (response.result.length === 0) return;

		let snippets = [];
		snippets = response.result.snippets;

		// Get all the <p> elements in the document
		const paragraphs = document.querySelectorAll("p, h1, h2, h3, h4, h5, h6, li, span, div");

		// Loop through all the <p> elements
		paragraphs.forEach((paragraph) => {
			// Loop through all the snippets
			snippets.forEach((snippet) => {
				// Highlight the text
				let regex = new RegExp(snippet, 'g');
				if (paragraph.textContent.match(regex)) {
					highlightText(paragraph, snippet.text);
				}
			});
		});
	}
	const displayBiasEditorPopup = (selection) => {
		// Create modal container
		const modalContainer = document.createElement("div");
		modalContainer.classList.add("mab-modal-container");

		// Modal content container
		const modalContent = document.createElement("div");
		modalContent.classList.add("mab-modal-content");

		// Modal close button
		const modalCloseButton = document.createElement("button");
		modalCloseButton.classList.add("mab-modal-close");
		modalCloseButton.innerHTML = "&times;";
		modalCloseButton.addEventListener("click", () => {
			document.body.removeChild(modalContainer); // Close modal on click
		});
		modalContent.appendChild(modalCloseButton);

		// Modal header
		const modalBiasHeader = document.createElement("h2");
		modalBiasHeader.innerHTML = "Mark text as biased";
		modalContent.appendChild(modalBiasHeader);

		// Modal bias text preview
		const modalBiasPreview = document.createElement("blockquote");
		modalBiasPreview.classList.add("mab-modal-bias-text-preview");
		modalBiasPreview.innerHTML = `"${selection.toString()}"`; // Preview highlighted text
		modalContent.appendChild(modalBiasPreview);

		// Modal form
		const modalForm = document.createElement("form");
		modalForm.classList.add("mab-modal-bias-form");

		// Form label and input for user comments
		const formLabel = document.createElement("label");
		formLabel.innerHTML = "Explain why this text is biased:";
		modalForm.appendChild(formLabel);

        // Textarea for the users note on why the text is biased
		const formTextArea = document.createElement("textarea");
		formTextArea.classList.add("mab-modal-bias-textarea");
		formTextArea.placeholder = "Enter your reasoning here...";
		formTextArea.rows = 4;
		modalForm.appendChild(formTextArea);

        // Hidden input for the selected text that will be sent to the server
		const formInput = document.createElement("input");
		formInput.classList.add("mab-modal-bias-input");
		formInput.id = "mab-modal-bias-input";
		formInput.type = "hidden";
		formInput.value = selection.toString();
		modalForm.appendChild(formInput);

		// Form submit button
		const formSubmit = document.createElement("button");
		formSubmit.classList.add("mab-modal-bias-submit");
		formSubmit.type = "submit";
		formSubmit.innerHTML = "Submit";
		modalForm.appendChild(formSubmit);

		// Append form to modal content
		modalContent.appendChild(modalForm);

		// Append content to modal container
		modalContainer.appendChild(modalContent);

		// Append modal to body
		document.body.appendChild(modalContainer);

		// Prevent form submission for this example
		modalForm.addEventListener("submit", (e) => {
			e.preventDefault();
			api.markAsBias(selection, window.location.href).then((response) => {
				highlightExistingBias();
				displayModalMessage("success", "Successfully posted data");
				console.info("Success posting data:", response);
			}).catch(error => {
				//	displayModalError();
				console.error("Error posting data:", error);
				displayModalMessage("error", "Error posting data");
			});

			document.body.removeChild(modalContainer);
		});
	};

	const generateBiasButtonPopup = (selection) => {
		// Create a new div element
		const popupContainer = document.createElement("div");
		popupContainer.setAttribute("id", "popup-container");

		// and give it some content
		const markAsBiasButton = document.createElement("button");
		markAsBiasButton.setAttribute("id", "mark-as-bias-button");
		markAsBiasButton.innerHTML = "Mark as Bias";
		markAsBiasButton.setAttribute('type', 'button');
		// add the text node to the newly created div
		popupContainer.appendChild(markAsBiasButton);

		const range = selection.getRangeAt(0);
		const rect = range.getBoundingClientRect();
		const top = rect.top + window.scrollY + rect.height + 10;
		const left = rect.left;

		popupContainer.style.top = `${top}px`;
		popupContainer.style.left = `${left}px`;
		popupContainer.style.display = "block";

		 // Add the newly created element and its content into the DOM
		 if (document.body.firstChild) {
			document.body.insertBefore(popupContainer, document.body.firstChild);
		} else {
			document.body.appendChild(popupContainer);
		}

		// Add event listener to the button
		document.querySelector("#mark-as-bias-button").addEventListener("click", () => {
			displayBiasEditorPopup(selection);
		});

		// Create a <style> element
		var style = document.createElement("style");

		// Define CSS rules
		style.textContent = `
			#popup-container {
				position: absolute;
				top: 0;
				right: 0;
				background-color: #f1f1f1;
				border: 1px solid #d3d3d3;
				z-index: 99999;
				width: 200px
			}

			#mark-as-bias-button {
				background-color: #4CAF50;
				color: white;
				padding: 14px 20px;
				border: none;
				cursor: pointer;
				width: 100%;
			}
		`;

		// Append the <style> element to the document
		document.head.appendChild(style);
	};

    /**
     * getSelection() - This function will get the selected text and display a popup below the selected text
     * @returns null
     */
	const getSelection = () => {
		let selectionText = document.getSelection().toString();
		const selection = document.getSelection();
		
		let previousTextSelection = "";

		// If the selection is not empty
		if (selectionText.length > 0) {
			if (previousTextSelection === selectionText) {
				removePopup();
				return false;
			}
			// check if there is already a popup and remove it if there is
			previousTextSelection = selectionText;
			// remove the old popup
			removePopup();
			// and generate a new popup below the new selected text
			generateBiasButtonPopup(selection);
            // store the selected text in the api property for later use
            api.setSelectionData();
		} else {
			removePopup();
		}
	}

	const removePopup = () => {
		if (document.querySelector("#popup-container")) {
			document.querySelector("#popup-container").remove();
		}
	}
	
	// check the message from the service worker. if the validUrl is false, then we don't want to run the content script
	chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
		if (message.validUrl === true) {
			// We want to check when someone highlights some text
			// and then shows a popup for that text to say "mark as bias"

			highlightExistingBias().then(() => {
                // get all the text that is highlighted
                let highlightedText = document.querySelectorAll('.mab-bias-highlight');
                highlightedText.forEach((highlightedText) => {
                    highlightedText.addEventListener('click', () => {
                        console.log("CLICKED ON HIGHLIGHTED TEXT")
                        displayBiasDetails(highlightedText.innerHTML);
                    });
                });
            });

			document.addEventListener("mouseup", function (event) {
				console.log("SELECTED SOME TEXT")
				if (event.target.tagName === "BUTTON") {
					return;
				}
				getSelection();
			});
		}
	});

    /**
     * displayBiasDetails - This function will display the bias details in a modal and allow the user to vote
     * 
     */
    const displayBiasDetails = (biasText) => {
        // Create modal container
        const modalContainer = document.createElement("div");
        modalContainer.classList.add("mab-modal-container");

        // Modal content container
        const modalContent = document.createElement("div");
        modalContent.classList.add("mab-modal-content");

        // Modal close button
        const modalCloseButton = document.createElement("button");
        modalCloseButton.classList.add("mab-modal-close");
        modalCloseButton.innerHTML = "&times;";
        modalCloseButton.addEventListener("click", () => {
            document.body.removeChild(modalContainer); // Close modal on click
        });
        modalContent.appendChild(modalCloseButton);

        // Modal header
        const modalBiasHeader = document.createElement("h2");
        modalBiasHeader.innerHTML = "Bias Details";
        modalContent.appendChild(modalBiasHeader);

        // Modal bias text preview
        const modalBiasPreview = document.createElement("blockquote");
        modalBiasPreview.classList.add("mab-modal-bias-text-preview");
        modalBiasPreview.innerHTML = biasText; // Preview highlighted text
        modalContent.appendChild(modalBiasPreview);

        // Modal form
        const modalForm = document.createElement("form");
        modalForm.classList.add("mab-modal-bias-form");

        // Form label and input for user comments
        const formLabel = document.createElement("label");
        formLabel.innerHTML = "Explain why this text is biased:";
        modalForm.appendChild(formLabel);

        // Textarea for the users note on why the text is biased
        const formTextArea = document.createElement("div");
        formTextArea.classList.add("mab-modal-bias-textarea");
        formTextArea.placeholder = "Enter your reasoning here...";
        formTextArea.rows = 4;
        modalForm.appendChild(formTextArea);

        // Form submit button
        const formSubmit = document.createElement("button");
        formSubmit.classList.add("mab-modal-bias-submit");
        formSubmit.type = "submit";
        formSubmit.innerHTML = "Submit";
        modalForm.appendChild(formSubmit);
    }

	const displayModalMessage = (messageType, message) => {
		// Create modal container
        const modalContainer = document.createElement("div");
        modalContainer.classList.add("mab-modal-container");

        // Modal content container
        const modalContent = document.createElement("div");
        modalContent.classList.add("mab-modal-content");
		if (messageType=='success') modalContent.classList.add("mab-modal-content--success");
		else modalContent.classList.add("mab-modal-content--error");

        // Modal close button
        const modalCloseButton = document.createElement("button");
        modalCloseButton.classList.add("mab-modal-close");
        modalCloseButton.innerHTML = "&times;";
        modalCloseButton.addEventListener("click", () => {
            document.body.removeChild(modalContainer); // Close modal on click
        });
        modalContent.appendChild(modalCloseButton);

		        // Textarea for the users note on why the text is biased
        const modalMessage = document.createElement("div");
        modalMessage.classList.add("mab-modal-message");
        modalMessage.innerText = message;
        modalContent.appendChild(modalMessage);
	}

    /////////////////////////
    /////////// Styles
    /////////////////////////
    
		// Style the modal (CSS)
		let style = document.createElement("style");
		style.textContent = `
			.mab-modal-container {
				position: fixed;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, 0.5);
				display: flex;
				align-items: center;
				justify-content: center;
				z-index: 9999999;
			}

			.mab-modal-content {
				background-color: #fff;
				padding: 20px;
				border-radius: 8px;
				box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
				width: 90%;
				max-width: 500px;
				position: relative;
			}

			.mab-modal-close {
				position: absolute;
				top: 10px;
				right: 10px;
				font-size: 20px;
				cursor: pointer;
				color: #888;
				background: none;
				border: none;
			}

			.mab-modal-close:hover {
				color: #000;
			}

			.mab-modal-bias-text-preview {
				font-style: italic;
				margin: 10px 0 20px;
				color: #333;
			}

			.mab-modal-bias-form label {
				font-weight: bold;
				display: block;
				margin-bottom: 8px;
			}

			.mab-modal-bias-textarea {
				width: 100%;
				padding: 8px;
				margin-bottom: 15px;
				border: 1px solid #ccc;
				border-radius: 4px;
				resize: vertical;
			}

			.mab-modal-bias-submit {
				background-color: #007bff;
				color: #fff;
				border: none;
				padding: 10px 20px;
				border-radius: 4px;
				cursor: pointer;
				font-size: 16px;
			}

			.mab-modal-bias-submit:hover {
				background-color: #0056b3;
			}

            .mab-bias-highlight {
                cursor: pointer;
                background-color: #ff00002b;
                transition: background-color 0.2s;
            }

            .mab-bias-highlight:hover {
                background-color: #ff7373;
            }

			.mab-modal-message-container {
				position: fixed;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, 0.5);
				display: flex;
				align-items: center;
				justify-content: center;
				z-index: 9999999;
			}

			.mab-modal-message-content {
				background-color: #fff;
				padding: 20px;
				border-radius: 8px;
				box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
				width: 90%;
				max-width: 500px;
				position: relative;
			}

			.mab-modal-message-close {
				position: absolute;
				top: 10px;
				right: 10px;
				font-size: 20px;
				cursor: pointer;
				color: #888;
				background: none;
				border: none;
			}

			.mab-modal-message--error {
				background-color:red;
			}
			.mab-modal-message--success {
				background-color:green;
			}
		`;
		document.head.appendChild(style);

})();
