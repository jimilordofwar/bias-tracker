(() => {
    chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
        if (message.validUrl === true) {
            let previousTextSelection = "";
            let snippets = [];
            const processedSnippets = new Set();

            const normalizeText = (html) => {
                var tempDiv = document.createElement("div");
                tempDiv.innerHTML = html;
                return tempDiv.textContent || tempDiv.innerText || "";
            };

            const wrapTextNode = (range, highlightClass) => {
                var span = document.createElement('span');
                span.className = highlightClass;
                span.appendChild(range.extractContents());
                range.insertNode(span);
            };

            const processChunk = (walker, normalizedText, highlightClass, updates, resolve) => {
                let node;
                let range = document.createRange();
                const chunkSize = 5; // Reduced chunk size
                let count = 0;

                while (node = walker.nextNode()) {
                    let startOffset = node.nodeValue.indexOf(normalizedText);
                    if (startOffset !== -1) {
                        let endOffset = startOffset + normalizedText.length;
                        range.setStart(node, startOffset);
                        range.setEnd(node, endOffset);
                        updates.push(range.cloneRange());
                    }

                    count++;
                    if (count >= chunkSize) {
                        requestAnimationFrame(() => processChunk(walker, normalizedText, highlightClass, updates, resolve));
                        return;
                    }
                }

                resolve();
            };

            const findAndHighlightText = (root, text, highlightClass) => {
                return new Promise((resolve) => {
                    let normalizedText = normalizeText(text);
                    let walker = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, null, false);
                    let updates = [];
                    processChunk(walker, normalizedText, highlightClass, updates, () => {
                        // Apply updates in batch
                        updates.forEach(range => {
                            console.log(`Highlighting range: ${range.toString()}`);
                            wrapTextNode(range, highlightClass);
                        });
                        resolve();
                    });
                });
            };

            const highlightExistingBias = async () => {
                const response = await fetch("http://localhost/index.php?biasUrl=" + window.location.href, {
                    method: "GET",
                    headers: {
                        "Content-Type": "",
                    },
                });

                if (!response.ok) {
                    return;
                }

                const data = await response.json();
                console.log(data);

                if (data.result.length === 0) return;

                snippets = data.result.snippets;

                const elements = document.querySelectorAll("p, h1, h2, h3, h4, h5, h6, li, span, div");
                for (const element of elements) {
                    for (const snippet of snippets) {
                        const snippetText = snippet.text;
                        if (!processedSnippets.has(snippetText)) {
                            console.log(`Highlighting snippet: ${snippetText}`);
                            await findAndHighlightText(element, snippetText, 'highlight');
                            processedSnippets.add(snippetText);
                        }
                    }
                }
            };

            highlightExistingBias();

            const generatePopup = (selection) => {
                const popupContainer = document.createElement("div");
                popupContainer.setAttribute("id", "popup-container");

                const markAsBiasButton = document.createElement("button");
                markAsBiasButton.setAttribute("id", "mark-as-bias-button");
                markAsBiasButton.innerHTML = "Mark as Bias";
                markAsBiasButton.setAttribute('type', 'button');
                popupContainer.appendChild(markAsBiasButton);

                const range = selection.getRangeAt(0);
                const rect = range.getBoundingClientRect();
                const top = rect.top + window.scrollY + rect.height + 10;
                const left = rect.left;

                popupContainer.style.top = `${top}px`;
                popupContainer.style.left = `${left}px`;
                popupContainer.style.display = "block";

                if (document.body.firstChild) {
                    document.body.insertBefore(popupContainer, document.body.firstChild);
                } else {
                    document.body.appendChild(popupContainer);
                }

                document.querySelector("#mark-as-bias-button").addEventListener("click", () => {
                    console.log("Mark as bias button clicked");
                    fetch("http://localhost/index.php", {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                            biasText: selection.toString(),
                            biasUrl: window.location.href,
                        }),

                    }).then((response) => {
                        response.json().then(data => {
                            console.log("Button Clicked and post complete", data);
                            document.querySelector("#popup-container").remove();
                            highlightExistingBias();
                        });
                    });
                });

                var style = document.createElement("style");
                style.textContent = `
                    #popup-container {
                        position: absolute;
                        top: 0;
                        right: 0;
                        background-color: #f1f1f1;
                        border: 1px solid #d3d3d3;
                        z-index: 1;
                        width: 200px;
                    }

                    button {
                        background-color: #4CAF50;
                        color: white;
                        padding: 14px 20px;
                        border: none;
                        cursor: pointer;
                        width: 100%;
                    }

                    .highlight {
                        background-color: yellow;
                    }
                `;
                document.head.appendChild(style);
            };

            document.addEventListener("mouseup", function (event) {
                let selectionText = document.getSelection().toString();
                const selection = document.getSelection();

                if (selectionText.length > 0) {
                    if (previousTextSelection === selectionText) {
                        return;
                    }
                    previousTextSelection = selectionText;
                    if (document.querySelector("#popup-container")) {
                        document.querySelector("#popup-container").remove();
                    }

                    generatePopup(selection);
                } else {
                    if (document.querySelector("#popup-container")) {
                        document.querySelector("#popup-container").remove();
                    }
                }
            });
        }
    });
})();
