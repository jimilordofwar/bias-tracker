(() => {

// service-worker.js

	// Use the *compat* versions since you're not using a module bundler.
	// Use *LOCAL* paths to the downloaded Firebase SDK files.
	//importScripts('firebase/firebase-app-compat.js');
	//importScripts('firebase/firebase-auth-compat.js');

	const firebaseConfig = {
		apiKey: "AIzaSyCGAZNolH2otTu-nDN_sK5IJ-xhckpInxo",
		authDomain: "bias-tracker-1030d.firebaseapp.com",
		projectId: "bias-tracker-1030d",
		storageBucket: "bias-tracker-1030d.firebasestorage.app",
		messagingSenderId: "792293521503",
		appId: "1:792293521503:web:9249908daddc727a25c3ea"
	};

	firebase.initializeApp(firebaseConfig);
	const auth = firebase.auth();

	chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
		console.log("HERE WE TRY TO SIGN IN", message.action);
		if (message.action === 'signInWithGoogle') {
			const provider = new firebase.auth.GoogleAuthProvider();
			auth.signInWithRedirect(provider); // Initiate the redirect
			// No need for then/catch here, as the redirect happens immediately
			sendResponse({ success: true }); //Acknowledge message has been received
			return true; //Keep channel open
		} else if (message.action === 'signOut') {
			auth.signOut()
				.then(() => {
					sendResponse({ success: true });
				})
				.catch(error => {
					sendResponse({ success: false, error: error.message });
				});
			return true; // Keep the message channel open.
		} else if (message.action === 'checkAuthStatus') {
			auth.onAuthStateChanged(user => {
				if (user) {
					user.getIdToken().then(idToken => {
						sendResponse({ isLoggedIn: true, idToken: idToken });
					});
				} else {
					sendResponse({ isLoggedIn: false });
				}
			});
			return true; // Keep the message channel open.
		}
	});

	chrome.runtime.onInstalled.addListener(({reason}) => {
		if (reason === 'install') {
			chrome.tabs.create({
			url: "onboarding.html"
			});
		}
	});


	let validUrls = [
		// USA
		"nytimes.com",
		"foxnews.com",
		"wsj.com",
		"npr.org",
		"apnews.com",
		"reuters.com",
		"theguardian.com",
		"bloomberg.com",
		"politico.com",
		"axios.com",
		"thehill.com",
		"usatoday.com",
		"latimes.com",
		"chicagotribune.com",
		"bostonglobe.com",
		// UK
		"jamesbiltaji.com",
		"bbc.co.uk",
		"dailymirror.co.uk",
		"thetimes.co.uk",
		"telegraph.co.uk",
		"express.co.uk",
		"mirror.co.uk",
		"standard.co.uk",
		"thesun.co.uk",
		"guardian.co.uk",
		"independent.co.uk",
		"metro.co.uk",
		"lbc.co.uk",
		// Global
		"aljazeera.com",
		"dw.com",
		"france24.com",
		"rt.com",
		"chinadaily.com",
		"japantimes.co.jp",
		"indiatoday.in",
		"thehindu.com",
		"aljazeera.net",
		"dw.de",
	];

	// Listen for tab updates (tab URL change or tab activation)
	chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
		if (changeInfo.status === 'complete' && tab.active) {
			handleTabUrl(tabId, tab.url);
		}
	});

	chrome.tabs.onActivated.addListener((activeInfo) => {
		chrome.tabs.get(activeInfo.tabId, (tab) => {
			handleTabUrl(tab.id, tab.url);
		});
	});

	function handleTabUrl(tabId, url) {
		validUrl = false;
		validUrls.forEach(website => {
			// regex to match the url of the tab with the valid urls
			let regex = new RegExp(website, 'g');
			if (url.match(regex)) {
				// console.log(tab.url);
				validUrl = true;
			}
		});

		chrome.tabs.sendMessage(tabId, { validUrl });
	}


})();