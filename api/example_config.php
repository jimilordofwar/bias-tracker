<?php
const servername = "MY_DB_IP";
const database = "DB_NAME";
const username = "DB_USERNAME";
const password = "DB_PASSWORD";

const apiUrl = "localhost/index.php";

$compatibleWebsites = [
	"nytimes.com",
	"foxnews.com",
	"wsj.com",
	"npr.org",
	"apnews.com",
	"reuters.com",
	"bbc.co.uk",
	"aljazeera.com",
	"theguardian.com",
	"bloomberg.com",
	"jamesbiltaji.com"
];