<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization");

require_once 'config.php';
require_once 'classes/db.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$data = json_decode(file_get_contents('php://input'), true);

	if (
		(isset($data['biasSelection']) && !empty($data['biasSelection']))
	 && (isset($data['biasUrl']) && !empty($data['biasUrl']))
	){
		$biasSelection = $data['biasSelection'];
		$biasUrl = $data['biasUrl'];

		$db = new DB();

		// check that the text is longer than 20 characters
		if (strlen($biasSelection['plainText']) < 20) {
			echo json_encode(['error' => 'Text must be at least 20 characters long']);
			die();
		}

		// check that the website is in the list of compatible websites
		if (! preg_match("/^https?:\/\/(www\.)?(".implode("|", $compatibleWebsites).")/", $biasUrl)) {
			echo json_encode(['error' => 'Website not supported']);
			die();
		}

		// check that the url is not already in the database
		$existingUrl = $db->fetchOne("SELECT * FROM articles WHERE url = ?", [$biasUrl]);

		// if the article didn't already exist then we know it's a new snippet
		// and we can add the snippet to the database
		if (! $existingUrl) {
			$db->execute("INSERT INTO articles SET url=?", [$biasUrl]);
			echo json_encode(['success' => 'Text added to database']);
			die();
		}
        
        $articleId = $existingUrl->id ?? $db->dbh->lastInsertId();
		// check that the text is not already in the database
		$existingText = $db->fetchOne("SELECT * FROM snippets WHERE text = ? AND article_id=?", [$biasSelection['plainText'], $existingUrl->id]);
		if ($existingText) {
			echo json_encode(['error' => 'Text already exists in the database']);
			die();
		}
		
        $db->execute("INSERT INTO snippets SET text=?, article_id=?, submitter_ip=?, note=?", [$biasSelection['plainText'], $articleId, $_SERVER['REMOTE_ADDR'], $biasSelection['note']]);
		echo json_encode(['success' => 'This text has been marked as bias']);
		die();
	}

	echo "Failed";
	die();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
	if (isset($_GET['biasUrl'])) {
		$db = new DB();
		$article = $db->fetchOne("SELECT * FROM articles WHERE url=?", [$_GET['biasUrl']]);

		if (!$article) {
			echo json_encode(['result' => '']);
			die();
		}

		$snippets = $db->fetchAll("SELECT * FROM snippets WHERE article_id=?", [$article->id]);

		echo json_encode([
			'result' => [
				"article" => $article,
				"snippets" => $snippets
			]
		]);
		die();
	}
}


