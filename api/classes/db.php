<?php

class DB {

	// connect to mysql db
	private $server = "192.168.0.221";
	private $database = "u699334307_bias_tracker";
	private $username = "u699334307_bias_tracker";
	private $password = "&Yg3Jk@J^t";

	public $dbh;

	function __construct() {
		$dsn = "mysql:host=$this->server;dbname=$this->database;charset=utf8mb4";
		$options = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		try {
			$this->dbh = new PDO($dsn, $this->username, $this->password, $options);
		} catch (\PDOException $e) {
			throw new \PDOException($e->getMessage(), (int)$e->getCode());
		}
	}

	function fetchOne($query = "", $params = []) {
		$statement = $this->dbh->prepare($query);
		$statement->execute($params);
		$result = $statement->fetch();
		return $result;
	}
	
	function fetchAll($query = "", $params = []) {
		$results = [];
		$statement = $this->dbh->prepare($query);
		$statement->execute($params);

		while ($result = $statement->fetch()) {
			$results[] = $result;
		}

		return $results;
	}

	function execute($query, $params= []) {
		$statement = $this->dbh->prepare($query);
		return $statement->execute($params);
	}


}